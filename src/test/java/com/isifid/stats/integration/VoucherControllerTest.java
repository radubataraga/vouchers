package com.isifid.stats.integration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.isifid.stats.domain.GroupBy;
import com.isifid.stats.domain.request.VoucherRequest;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.isifid.stats.utils.Constants.USE_ONE_ENDPOINT;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class VoucherControllerTest {

	protected MediaType contentType;
	protected MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		contentType = new MediaType(MediaType.APPLICATION_JSON.getType(), MediaType.APPLICATION_JSON.getSubtype());
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	@Test
	@Ignore
	public void loadOneTest() throws Exception {
		VoucherRequest voucher = new VoucherRequest();
		voucher.setVoucher_type(0);
		voucher.setGroup_by(GroupBy.YEAR);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		voucher.setDate_max(new Date());
		voucher.setDate_min(new Date());
		voucher.setPartner_id(1);
		Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		String json = gson.toJson(voucher);

		ResultActions succesfulRegistration = mockMvc.perform(MockMvcRequestBuilders.post(USE_ONE_ENDPOINT).contentType(contentType).content(json));
		succesfulRegistration.andExpect(status().isOk());
		succesfulRegistration.andExpect(jsonPath("$.partner_id", Matchers.is(1)));
	}
}
