package com.isifid.stats.integration;

import com.isifid.stats.entity.Item;
import com.isifid.stats.entity.VoucherType;
import com.isifid.stats.repository.ItemRepository;
import com.isifid.stats.repository.VoucherTypeRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class RepositoryTests {

	@Autowired
	private VoucherTypeRepository voucherTypeRepository;

	@Autowired
	private ItemRepository itemRepository;

	@Test
	@Ignore
	public void insert() {
		VoucherType voucherType = new VoucherType();
		voucherTypeRepository.save(voucherType);
	}

	@Test
	@Ignore
	public void insertItem() {
		Item item = new Item();
		item.setProduct_id(1);
		item.setUse_date(new Date());
		itemRepository.save(item);
	}

	@Test
	@Ignore
	public void find() {
		Optional<Item> foundItem = itemRepository.findById(2);
		System.out.println(foundItem.get());
	}
}
