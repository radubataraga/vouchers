package com.isifid.stats.integration;

import com.isifid.stats.entity.Partner;
import com.isifid.stats.repository.PartnerRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@Ignore
public class PartnerRepositoryTest {

	@Autowired
	private PartnerRepository partnerRepository;

	@Test
	@Ignore
	//This should fail because no @Transcation is used.
	public void find() {
		Optional<Partner> partner = partnerRepository.findById(1);
	}
}
