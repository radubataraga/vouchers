package com.isifid.stats.controller;

import com.isifid.stats.domain.GroupBy;
import com.isifid.stats.domain.request.AllVoucherRequest;
import com.isifid.stats.domain.response.AllVouchersResponse;
import com.isifid.stats.domain.response.PartnerVoucherResponse;
import com.isifid.stats.domain.request.VoucherRequest;
import com.isifid.stats.service.PartnerVoucherService;
import com.isifid.stats.service.exception.PartnerException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;

import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VoucherControllerTest {
	//TODO : see if there's a better runner for jdk11

	@Mock
	private PartnerVoucherService partnerVoucherService;

	@Mock
	private PartnerVoucherResponse partnerVoucherResponse;

	@Mock
	private AllVouchersResponse allVouchersResponse;

	@InjectMocks
	public VoucherController voucherController;

	@Test
	public void useVoucherTest() throws PartnerException {
		VoucherRequest voucherRequest = new VoucherRequest();
		voucherRequest.setDate_max(new Date());
		voucherRequest.setDate_min(new Date());
		voucherRequest.setGroup_by(GroupBy.MONTH);
		voucherRequest.setVoucher_type(0);
		when(partnerVoucherService.processVoucher(any(VoucherRequest.class))).thenReturn(partnerVoucherResponse);
		assertEquals(HttpStatus.OK, voucherController.useVoucher(voucherRequest).getStatusCode());
		assertEquals(partnerVoucherResponse, voucherController.useVoucher(voucherRequest).getBody());
	}

	@Test
	public void useAllVoucherTest() {
		AllVoucherRequest voucherRequest = new AllVoucherRequest();
		voucherRequest.setDate_max(new Date());
		voucherRequest.setDate_min(new Date());
		voucherRequest.setGroup_by(GroupBy.MONTH);
		voucherRequest.setVoucher_type(0);


		when(partnerVoucherService.processVoucher(any(AllVoucherRequest.class))).thenReturn(allVouchersResponse);

		assertEquals(HttpStatus.OK, voucherController.useAllVoucher(voucherRequest).getStatusCode());
		assertEquals(allVouchersResponse, voucherController.useAllVoucher(voucherRequest).getBody());
	}
}
