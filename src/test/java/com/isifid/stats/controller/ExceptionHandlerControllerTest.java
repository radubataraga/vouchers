package com.isifid.stats.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import java.util.ArrayList;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

// annotatie pentru runner, JUnit runner 4,
// pentru integration testing este folosit SpringRunnerul.

@RunWith(MockitoJUnitRunner.class)
public class ExceptionHandlerControllerTest {

    @Mock
    private MethodArgumentNotValidException methodArgumentMock;

    @Mock
    private BindingResult bindingResultMock;

    @InjectMocks
    private ExceptionHandlerController exceptionHandlerController = new ExceptionHandlerController();

    @Test
    public void inputValidationError() {
        ResponseEntity<String> result = exceptionHandlerController.inputValidationError();
        assertNotNull(result);
    }

    @Test
    public void handleValidationExceptions() {
        when(methodArgumentMock.getBindingResult()).thenReturn(bindingResultMock);
        when(bindingResultMock.getAllErrors()).thenReturn(new ArrayList<ObjectError>());

        ResponseEntity<Map<String, String>> result = exceptionHandlerController.handleValidationExceptions(methodArgumentMock);
        assertNotNull(result);
        assertEquals(result.getBody().size(), 0);
    }

    @Test
    public void handleValidationOneErrorExceptions() {
        when(methodArgumentMock.getBindingResult()).thenReturn(bindingResultMock);

        ArrayList<ObjectError> errors = new ArrayList<ObjectError>();
        errors.add(new ObjectError("asd","asd"));

        when(bindingResultMock.getAllErrors()).thenReturn(errors);

        ResponseEntity<Map<String, String>> result = exceptionHandlerController.handleValidationExceptions(methodArgumentMock);

        assertNotNull(result);
        assertEquals(result.getBody().size(), 1);
    }

    @Test
    public void handleValidationOneFieldrrorExceptions() {
        when(methodArgumentMock.getBindingResult()).thenReturn(bindingResultMock);

        ArrayList<ObjectError> errors = new ArrayList<ObjectError>();
        errors.add(new FieldError("asd","asd","asd"));

        when(bindingResultMock.getAllErrors()).thenReturn(errors);

        ResponseEntity<Map<String, String>> result = exceptionHandlerController.handleValidationExceptions(methodArgumentMock);

        assertNotNull(result);
        assertEquals(result.getBody().size(), 1);
    }
}