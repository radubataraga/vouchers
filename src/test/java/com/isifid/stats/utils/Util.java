package com.isifid.stats.utils;

import com.isifid.stats.domain.GroupBy;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class Util {

	public static Date getDate(String dateInString) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = sdf.parse(dateInString);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	@Test
	public void extractDateByGroupTest() {
		Date minDate = Util.getDate("2000-08-31");

		assertEquals(Integer.valueOf(2000), Utils.extractDateByGroup(minDate, GroupBy.YEAR));
		assertEquals(Integer.valueOf(8), Utils.extractDateByGroup(minDate, GroupBy.MONTH));
		assertEquals(Integer.valueOf(31), Utils.extractDateByGroup(minDate, GroupBy.DAY));
	}

	@Test
	public void getVoucherTypeTest() {
		assertEquals(Integer.valueOf(0), Utils.getVoucherType(Integer.valueOf(1), Boolean.valueOf(false)));
		assertEquals(Integer.valueOf(1), Utils.getVoucherType(Integer.valueOf(1), Boolean.valueOf(true)));
		assertEquals(Integer.valueOf(2), Utils.getVoucherType(Integer.valueOf(2), Boolean.valueOf(false)));
		assertEquals(Integer.valueOf(2), Utils.getVoucherType(Integer.valueOf(2), Boolean.valueOf(true)));
		assertEquals(Integer.valueOf(2), Utils.getVoucherType(Integer.valueOf(2), null));
	}
}
