package com.isifid.stats.service.impl;

import com.isifid.stats.domain.GroupBy;
import com.isifid.stats.domain.request.AllVoucherRequest;
import com.isifid.stats.domain.response.Voucher;
import com.isifid.stats.domain.request.VoucherRequest;
import com.isifid.stats.entity.Item;
import com.isifid.stats.entity.Partner;
import com.isifid.stats.entity.Product;
import com.isifid.stats.service.exception.PartnerException;
import com.isifid.stats.utils.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.isifid.stats.utils.TestConstants.maxDate;
import static com.isifid.stats.utils.TestConstants.minDate;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PartnerVoucherResponseServiceImplTest {

	@Mock
	private PartnerServiceImpl partnerService;

	@Mock
	private VoucherServiceImpl voucherService;

	@InjectMocks
	public PartnerVoucherServiceImpl partnerVoucherService;

	@Test
	public void processVoucher() throws PartnerException {
		VoucherRequest voucherRequest = new VoucherRequest();
		voucherRequest.setPartner_id(1);
		voucherRequest.setDate_min(minDate);
		voucherRequest.setDate_max(maxDate);
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);

		Set<Item> itemsList = new HashSet<>();
		Set<Product> products = new HashSet<>();
		Item item1 = new Item();
		item1.setUse_date(Util.getDate("2008-08-31"));
		Item item2 = new Item();
		item2.setUse_date(Util.getDate("2010-08-10"));
		itemsList.add(item1);
		itemsList.add(item2);

		Product product1 = new Product();
		product1.setItems(itemsList);
		product1.setStatus("ACTIVE_PRODUCTS");
		product1.setAmount(10.0);

		Product product2 = new Product();
		product2.setItems(itemsList);
		product2.setStatus("ACTIVE_PRODUCTS");
		product2.setAmount(10.0);

		Partner partner = new Partner();
		products.add(product1);
		products.add(product2);
		partner.setProducts(products);

		List<Voucher> firstList = new ArrayList<>();
		List<Voucher> secondList = new ArrayList<>();
		Voucher voucher1 = new Voucher();
		Voucher voucher2 = new Voucher();
		firstList.add(voucher1);
		firstList.add(voucher2);
		secondList.add(voucher2);
		when(voucherService.processProductVouchers(any(), any(), anyString())).thenReturn(firstList, secondList);
		when(partnerService.findPartnerById(anyInt())).thenReturn(partner);
		partnerVoucherService.processVoucher(voucherRequest);
	}

	@Test(expected = PartnerException.class)
	public void processVoucherWithBadPartnerId() throws PartnerException {
		VoucherRequest voucherRequest = new VoucherRequest();
		voucherRequest.setPartner_id(1);
		voucherRequest.setDate_min(minDate);
		voucherRequest.setDate_max(maxDate);
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);
		partnerVoucherService.processVoucher(voucherRequest);
	}

	@Test
	public void processAllVoucherWithOnePartnerWithVoucherTest() {
		AllVoucherRequest voucherRequest = new AllVoucherRequest();
		voucherRequest.setDate_min(minDate);
		voucherRequest.setDate_max(maxDate);
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);

		Set<Product> products = new HashSet<>();
		Set<Item> itemsList = new HashSet<>();
		Item item1 = new Item();
		item1.setUse_date(Util.getDate("2008-08-31"));
		Item item2 = new Item();
		item2.setUse_date(Util.getDate("2010-08-10"));
		itemsList.add(item1);
		itemsList.add(item2);

		Product product1 = new Product();
		Partner partner = new Partner();
		product1.setItems(itemsList);
		product1.setStatus("ACTIVE_PRODUCTS");
		product1.setAmount(10.0);
		products.add(product1);
		partner.setProducts(products);

		when(partnerService.findAllPartners()).thenReturn(Collections.singletonList(partner));
		when(voucherService.processProductVouchers(any(), any(), any())).thenReturn(Collections.singletonList(new Voucher()));
		assertEquals(0, partnerVoucherService.processVoucher(voucherRequest).getPartners().get(0).getPartner_id());
		assertEquals(GroupBy.YEAR, partnerVoucherService.processVoucher(voucherRequest).getPartners().get(0).getGroup_by());
		assertThat(partnerVoucherService.processVoucher(voucherRequest).getPartners().get(0).getVouchers(), hasSize(1));
	}

	@Test
	public void processAllVoucherWithOnePartnerWithoutVoucherTest() {
		AllVoucherRequest voucherRequest = new AllVoucherRequest();
		voucherRequest.setDate_min(minDate);
		voucherRequest.setDate_max(maxDate);
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);
		when(partnerService.findAllPartners()).thenReturn(Collections.singletonList(new Partner()));
		assertThat(partnerVoucherService.processVoucher(voucherRequest).getPartners(), hasSize(0));
	}

	@Test
	public void processAllVoucherWihoutPartnersTest() {
		AllVoucherRequest voucherRequest = new AllVoucherRequest();
		voucherRequest.setDate_min(minDate);
		voucherRequest.setDate_max(maxDate);
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);
		when(partnerService.findAllPartners()).thenReturn(Collections.emptyList());
		assertEquals(Collections.emptyList(), partnerVoucherService.processVoucher(voucherRequest).getPartners());
	}
}