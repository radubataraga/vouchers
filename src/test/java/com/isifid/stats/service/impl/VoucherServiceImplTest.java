package com.isifid.stats.service.impl;

import com.isifid.stats.domain.GroupBy;
import com.isifid.stats.domain.response.Voucher;
import com.isifid.stats.domain.request.VoucherRequest;
import com.isifid.stats.entity.Item;
import com.isifid.stats.entity.Product;
import com.isifid.stats.utils.Util;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.isifid.stats.utils.TestConstants.maxDate;
import static com.isifid.stats.utils.TestConstants.minDate;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class VoucherServiceImplTest {

	@Mock
	private PartnerServiceImpl partnerService;

	@InjectMocks
	public VoucherServiceImpl voucherService;

	@Test
	public void processVoucherByYearWithDifferentYearItems() {

		Set<Item> itemsList = new HashSet<>();
		Item item1 = new Item();
		item1.setUse_date(Util.getDate("2008-08-31"));
		Item item2 = new Item();
		item2.setUse_date(Util.getDate("2010-08-10"));
		itemsList.add(item1);
		itemsList.add(item2);

		Product product = new Product();
		product.setItems(itemsList);
		product.setStatus("ACTIVE");
		product.setAmount(10d);
		product.setVoucherTypeId(1);

		VoucherRequest voucherRequest = new VoucherRequest();
		voucherRequest.setPartner_id(1);
		voucherRequest.setDate_min(minDate);
		voucherRequest.setDate_max(maxDate);
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);
		List<Voucher> voucherList = voucherService.processProductVouchers(voucherRequest, product, "ACTIVE");
		assertThat(voucherList, hasSize(2));
		assertEquals(voucherList.get(0).getVoucher_amount(), 10, 0);
		assertEquals(voucherList.get(0).getVoucher_use(), Integer.valueOf(1));
		assertEquals(voucherList.get(0).getVoucher_amount(), voucherList.get(1).getVoucher_amount(), 0);
	}

	@Test
	public void processVoucherByYearWithSameYearItems() {

		Set<Item> itemsList = new HashSet<>();
		Item item1 = new Item();
		item1.setUse_date(Util.getDate("2011-08-31"));
		Item item2 = new Item();
		item2.setUse_date(Util.getDate("2011-08-10"));
		itemsList.add(item1);
		itemsList.add(item2);

		Product product = new Product();
		product.setItems(itemsList);
		product.setStatus("ACTIVE");
		product.setAmount(10d);
		product.setVoucherTypeId(1);

		VoucherRequest voucherRequest = new VoucherRequest();
		voucherRequest.setPartner_id(1);
		voucherRequest.setDate_min(minDate);
		voucherRequest.setDate_max(maxDate);
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);

		when(partnerService.findCofinanceRate(any(), any())).thenReturn(10);

		List<Voucher> voucherList = voucherService.processProductVouchers(voucherRequest, product, "ACTIVE");
		assertThat(voucherList, hasSize(1));
		assertEquals(voucherList.get(0).getVoucher_amount(), 10, 0);
		assertEquals(voucherList.get(0).getVoucher_use(), Integer.valueOf(2));
	}

	@Test
	public void processVoucherWithNoItems() {
		Product product = new Product();
		product.setStatus("ACTIVE");
		product.setVoucherTypeId(1);
		VoucherRequest voucherRequest = new VoucherRequest();
		voucherRequest.setPartner_id(1);
		voucherRequest.setDate_min(new Date());
		voucherRequest.setDate_max(new Date());
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);
		List<Voucher> voucherList = voucherService.processProductVouchers(voucherRequest, product, "ACTIVE");
		assertThat(voucherList, hasSize(0));
	}

	@Test
	public void processVoucherWithoutActiveState() {
		Product product = new Product();
		product.setStatus("NOT_ACTIVE_PRODUCTS");
		product.setVoucherTypeId(1);
		VoucherRequest voucherRequest = new VoucherRequest();
		voucherRequest.setPartner_id(1);
		voucherRequest.setDate_min(new Date());
		voucherRequest.setDate_max(new Date());
		voucherRequest.setGroup_by(GroupBy.YEAR);
		voucherRequest.setVoucher_type(0);
		List<Voucher> voucherList = voucherService.processProductVouchers(voucherRequest, product, "ACTIVE_PRODUCTS");
		assertTrue(voucherList.isEmpty());
	}
}