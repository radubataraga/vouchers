package com.isifid.stats.service.impl;

import com.isifid.stats.domain.request.BaseVoucherRequest;
import org.junit.Test;

import static com.isifid.stats.utils.TestConstants.maxDate;
import static com.isifid.stats.utils.TestConstants.minDate;
import static org.junit.Assert.*;

public class ConsistentDateParameterValidatorTest {

	@Test
	public void isValidTest() {
		BaseVoucherRequest baseVoucherRequest = new BaseVoucherRequest();
		ConsistentDateParameterValidator consistentDateParameterValidator = new ConsistentDateParameterValidator();
		consistentDateParameterValidator.isValid(baseVoucherRequest,null);

		baseVoucherRequest.setDate_max(maxDate);
		baseVoucherRequest.setDate_min(minDate);
		assertTrue(consistentDateParameterValidator.isValid(baseVoucherRequest,null));
	}

	@Test
	public void isValidWithReverseDatesTest(){
		BaseVoucherRequest baseVoucherRequest = new BaseVoucherRequest();
		ConsistentDateParameterValidator consistentDateParameterValidator = new ConsistentDateParameterValidator();
		baseVoucherRequest.setDate_max(minDate);
		baseVoucherRequest.setDate_min(maxDate);
		assertFalse(consistentDateParameterValidator.isValid(baseVoucherRequest,null));
	}

	@Test
	public void isValidWithNullDatesTest(){
		BaseVoucherRequest baseVoucherRequest = new BaseVoucherRequest();
		ConsistentDateParameterValidator consistentDateParameterValidator = new ConsistentDateParameterValidator();
		consistentDateParameterValidator.isValid(baseVoucherRequest,null);

		assertFalse(consistentDateParameterValidator.isValid(baseVoucherRequest,null));
	}
}