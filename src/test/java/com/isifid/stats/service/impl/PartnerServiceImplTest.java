package com.isifid.stats.service.impl;

import com.isifid.stats.entity.Partner;
import com.isifid.stats.entity.Partnership;
import com.isifid.stats.repository.PartnerRepository;
import com.isifid.stats.repository.PartnershipRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PartnerServiceImplTest {

	@Mock
	private PartnerRepository partnerRepository;

	@Mock
	private PartnershipRepository partnershipRepository;

	@InjectMocks
	private PartnerServiceImpl partnerServiceImpl;

	@Test
	public void findNoPartnerByIdTest() {
		Partner partner = new Partner();
		when(partnerRepository.findById(any())).thenReturn(Optional.of(partner));
		assertEquals(partner, partnerServiceImpl.findPartnerById(1));
	}

	@Test
	public void findPartnerByIdTest() {
		when(partnerRepository.findById(any())).thenReturn(Optional.empty());
		assertNull(partnerServiceImpl.findPartnerById(1));
	}

	@Test
	public void findAllPartnersTest() {
		Partner partner = new Partner();
		List<Partner> list = Collections.singletonList(partner);
		when(partnerRepository.findAll()).thenReturn(list);
		assertEquals(list, partnerServiceImpl.findAllPartners());
	}

	@Test
	public void findCofinanceRateTest() {
		Partnership partnership = new Partnership();
		partnership.setFundingRate(10);
		partnership.setFundingRatePremium(20);
		when(partnershipRepository.findByPartnerId(any())).thenReturn(Optional.of(partnership));
		assertEquals(Integer.valueOf(10), partnerServiceImpl.findCofinanceRate(1, 0));
	}

	@Test
	public void findPremiumCofinanceRateTest() {
		Partnership partnership = new Partnership();
		partnership.setFundingRate(10);
		partnership.setFundingRatePremium(20);
		when(partnershipRepository.findByPartnerId(any())).thenReturn(Optional.of(partnership));
		assertEquals(Integer.valueOf(20), partnerServiceImpl.findCofinanceRate(1, 2));
	}

	@Test
	public void findCofinanceRateWithoutPartnerTest() {
		assertNull(partnerServiceImpl.findCofinanceRate(1, 0));
	}
}