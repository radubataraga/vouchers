package com.isifid.stats.service;

import com.isifid.stats.entity.Partner;

import java.util.List;

public interface PartnerService {

	/**
	 * Find a partner depending on the partnerId.
	 * @param id the partner id
	 * @return the partner found, null if no partner found
	 */
	public Partner findPartnerById(int id);

	/**
	 * Find all the vouchers
	 * @return a list of all the partners
	 */
	List<Partner> findAllPartners();

	/**
	 * Obtains the cofinance rate depending on the voucher type for a partner.
	 * @param partnerId the id of the partner that has the voucher
	 * @param databaseVoucherType the voucher type
	 * @return the cofinance rate
	 */
	public Integer findCofinanceRate(Integer partnerId, Integer databaseVoucherType);
}
