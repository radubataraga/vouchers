package com.isifid.stats.service.impl;

import com.isifid.stats.entity.Partner;
import com.isifid.stats.entity.Partnership;
import com.isifid.stats.repository.PartnerRepository;
import com.isifid.stats.repository.PartnershipRepository;
import com.isifid.stats.service.PartnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PartnerServiceImpl implements PartnerService {

	@Autowired
	private PartnerRepository partnerRepository;

	@Autowired
	private PartnershipRepository partnershipRepository;

	//	@Transactional Transaction + lazy lazy fetch fetch
	// or make eager for Partnet and lazy for the other list
	@Override
	public Partner findPartnerById(int id) {
		Partner partner = null;
		Optional<Partner> optionalPartner = partnerRepository.findById(id);
		if (optionalPartner.isPresent()) {
			partner = optionalPartner.get();
		}
		return partner;
	}

	@Override
	public List<Partner> findAllPartners() {
		return partnerRepository.findAll();
	}

	public Integer findCofinanceRate(Integer partnerId, Integer databaseVoucherType) {
		Integer cofinanceRate = null;
		Partnership partnership;
		if (partnerId != null) {
			Optional<Partnership> optionalPartner = partnershipRepository.findByPartnerId(partnerId);
			if (optionalPartner.isPresent() && databaseVoucherType != null) {
				partnership = optionalPartner.get();
				if (databaseVoucherType == 0 || databaseVoucherType == 1) {
					cofinanceRate = partnership.getFundingRate();
				} else {
					cofinanceRate = partnership.getFundingRatePremium();
				}
			}
		}
		return cofinanceRate;
	}
}
