package com.isifid.stats.service.impl;

import com.isifid.stats.domain.request.BaseVoucherRequest;
import com.isifid.stats.domain.request.ConsistentDateParameters;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ConsistentDateParameterValidator implements ConstraintValidator<ConsistentDateParameters, BaseVoucherRequest> {

	/**
	 * Validates the input min and max date
	 * @param baseVoucherRequest the query requests from /use/one and /use/all endpoints
	 * @param context the validator context
	 * @return if the date inputs are correct
	 */
	@Override
	public boolean isValid(BaseVoucherRequest baseVoucherRequest, ConstraintValidatorContext context) {
       return (baseVoucherRequest.getDate_min() != null && baseVoucherRequest.getDate_max() != null) && (
               baseVoucherRequest.getDate_min().compareTo(baseVoucherRequest.getDate_max()) < 0);
    }
}
