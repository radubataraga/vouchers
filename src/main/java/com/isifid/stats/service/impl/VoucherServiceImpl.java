package com.isifid.stats.service.impl;

import com.isifid.stats.domain.request.BaseVoucherRequest;
import com.isifid.stats.domain.GroupBy;
import com.isifid.stats.domain.response.Voucher;
import com.isifid.stats.entity.Item;
import com.isifid.stats.entity.Product;
import com.isifid.stats.service.PartnerService;
import com.isifid.stats.service.VoucherService;
import com.isifid.stats.utils.Utils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.isifid.stats.utils.Constants.DATE_FORMAT;
import static com.isifid.stats.utils.Utils.extractDateByGroup;
import static com.isifid.stats.utils.Utils.isFilteredByVoucherType;

@Service
public class VoucherServiceImpl implements VoucherService {

	@Autowired
	private PartnerService partnerService;

	//	@Transactional(Transactional.TxType.SUPPORTS) // eager fetch + transcational support

	@Override
	public List<Voucher> processProductVouchers(BaseVoucherRequest voucherRequest, Product product, String acceptedStatus) {
		List<Voucher> voucherList = new ArrayList<>();
		if (StringUtils.isBlank(acceptedStatus) || acceptedStatus.equals(product.getStatus())) {
			voucherList = processProductVouchers(voucherRequest, product, voucherList);
		}
		return voucherList;
	}

	private List<Voucher> processProductVouchers(BaseVoucherRequest voucherRequest, Product product, List<Voucher> voucherList) {
		Map<Integer, List<Date>> dateGroupedMap = new HashMap<>();

		Date maxDateQuery = voucherRequest.getDate_max();
		Date minDateQuery = voucherRequest.getDate_min();
		GroupBy requestGroupBy = voucherRequest.getGroup_by();
		Integer voucherType = voucherRequest.getVoucher_type();
		Set<Item> itemList = product.getItems();
		Integer databaseVoucherType = Utils.getVoucherType(product.getVoucherTypeId(), product.getMaxiBon());

		for (Item item : itemList) {
			Date usedDate = item.getUse_date();
			if (usedDate != null && (usedDate.compareTo(minDateQuery) > 0) && (usedDate.compareTo(maxDateQuery) < 0)) {
				if (isFilteredByVoucherType(voucherType, databaseVoucherType)) {
					Integer category = extractDateByGroup(usedDate, requestGroupBy);
					populateMapWithTimeKeysAndListOfDates(item, dateGroupedMap, category);
				}
			}

		}
		return getVouchersFromTimeMap(product, voucherList, dateGroupedMap, databaseVoucherType);
	}

	private List<Voucher> getVouchersFromTimeMap(Product product, List<Voucher> voucherList, Map<Integer, List<Date>> dateGroupedMap,
			Integer databaseVoucherType) {
		for (Map.Entry<Integer, List<Date>> entry : dateGroupedMap.entrySet()) {
			Voucher voucher = new Voucher();
			List<Date> voucherDate = entry.getValue();
			voucher.setVoucher_amount(product.getAmount());
			voucher.setVoucher_use(voucherDate.size());
			voucher.setCofinancing_rate(partnerService.findCofinanceRate(product.getPartnerId(), databaseVoucherType));
			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
			voucher.setDate(sdf.format(voucherDate.get(0)));
			voucher.setVoucher_type(databaseVoucherType);
			voucherList.add(voucher);
		}
		return voucherList;
	}

	private void populateMapWithTimeKeysAndListOfDates(Item item, Map<Integer, List<Date>> dateGroupedMap, Integer category) {

		Date usedDate = item.getUse_date();
		if (usedDate != null && category != null) {
			if (dateGroupedMap.containsKey(category)) {
				List<Date> dateList = dateGroupedMap.get(category);
				dateList.add(usedDate);
			} else {
				List<Date> dateList = new ArrayList<>();
				dateList.add(usedDate);
				dateGroupedMap.put(category, dateList);
			}
		}

	}
}
