package com.isifid.stats.service.impl;

import com.isifid.stats.domain.request.AllVoucherRequest;
import com.isifid.stats.domain.request.VoucherRequest;
import com.isifid.stats.domain.response.AllVouchersResponse;
import com.isifid.stats.domain.response.PartnerVoucherResponse;
import com.isifid.stats.service.exception.PartnerException;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import static com.isifid.stats.utils.Constants.ALL_VOUCHERS_CACHE_NAME;
import static com.isifid.stats.utils.Constants.ONE_VOUCHER_CACHE_NAME;

@Service("cacheablePartnerVoucherService")
public class CacheablePartnerVoucherServiceImpl extends PartnerVoucherServiceImpl {


	@Cacheable(value = ONE_VOUCHER_CACHE_NAME, sync = true)
	@Override
	public PartnerVoucherResponse processVoucher(VoucherRequest voucherRequest) throws PartnerException {
		return super.processVoucher(voucherRequest);
	}

	@Cacheable(value = ALL_VOUCHERS_CACHE_NAME, sync = true)
	@Override
	public AllVouchersResponse processVoucher(AllVoucherRequest voucherRequest) {
		return super.processVoucher(voucherRequest);
	}
}
