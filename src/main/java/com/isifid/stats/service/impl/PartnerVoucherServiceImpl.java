package com.isifid.stats.service.impl;

import com.isifid.stats.domain.response.AllVouchersResponse;
import com.isifid.stats.domain.request.AllVoucherRequest;
import com.isifid.stats.domain.request.BaseVoucherRequest;
import com.isifid.stats.domain.response.PartnerVoucherResponse;
import com.isifid.stats.domain.response.Voucher;
import com.isifid.stats.domain.request.VoucherRequest;
import com.isifid.stats.entity.Partner;
import com.isifid.stats.entity.Product;
import com.isifid.stats.service.PartnerService;
import com.isifid.stats.service.PartnerVoucherService;
import com.isifid.stats.service.VoucherService;
import com.isifid.stats.service.exception.PartnerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.isifid.stats.utils.Constants.ACTIVE_PRODUCTS;

@Service("voucherService")
public class PartnerVoucherServiceImpl implements PartnerVoucherService {

	@Autowired
	private VoucherService voucherService;

	@Autowired
	private PartnerService partnerService;

	@Override
	public PartnerVoucherResponse processVoucher(VoucherRequest voucherRequest) throws PartnerException {

		PartnerVoucherResponse partnerVoucherResponse = new PartnerVoucherResponse();
		partnerVoucherResponse.setPartner_id(voucherRequest.getPartner_id());
		partnerVoucherResponse.setGroup_by(voucherRequest.getGroup_by());
		Partner partner = partnerService.findPartnerById(voucherRequest.getPartner_id());

		if (partner != null) {
			partnerVoucherResponse.setPartner_name(partner.getName());
			List<Voucher> voucherList = processProductsOfAPartner(voucherRequest, partner, ACTIVE_PRODUCTS);
			partnerVoucherResponse.setVouchers(voucherList);
		} else {
			throw new PartnerException("no partner with this id");
		}
		return partnerVoucherResponse;
	}

	@Override
	public AllVouchersResponse processVoucher(AllVoucherRequest voucherRequest) {

		AllVouchersResponse allVouchersResponse = new AllVouchersResponse();
		List<PartnerVoucherResponse> partnerVoucherResponseList = new ArrayList<>();
		List<Partner> partnersList = partnerService.findAllPartners();
		if (partnersList != null) {
			for (Partner partner : partnersList) {
				List<Voucher> voucherListOfAProduct = processProductsOfAPartner(voucherRequest, partner, voucherRequest.getStatus_id());
				if(voucherListOfAProduct.size()>0){
					PartnerVoucherResponse partnerVoucherResponse = new PartnerVoucherResponse();
					partnerVoucherResponse.setPartner_id(partner.getId());
					partnerVoucherResponse.setGroup_by(voucherRequest.getGroup_by());
					partnerVoucherResponse.setPartner_name(partner.getName());
					partnerVoucherResponse.setVouchers(voucherListOfAProduct);
					partnerVoucherResponseList.add(partnerVoucherResponse);
				}
			}
		}
		allVouchersResponse.setPartners(partnerVoucherResponseList);
		return allVouchersResponse;
	}

	private List<Voucher> processProductsOfAPartner(BaseVoucherRequest voucherRequest, Partner partner, String acceptedStatus) {
		Set<Product> productList = partner.getProducts();
		List<Voucher> voucherList = new ArrayList<>();
		for (Product product : productList) {
			List<Voucher> partnerVoucherList = voucherService.processProductVouchers(voucherRequest, product, acceptedStatus);
			if (!partnerVoucherList.isEmpty()) {
				voucherList.addAll(partnerVoucherList);
			}
		}
		return voucherList;
	}
}
