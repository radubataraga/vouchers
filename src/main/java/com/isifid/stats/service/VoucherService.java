package com.isifid.stats.service;

import com.isifid.stats.domain.request.BaseVoucherRequest;
import com.isifid.stats.domain.response.Voucher;
import com.isifid.stats.entity.Product;

import java.util.List;

public interface VoucherService {

	/**
	 * Process all the items of a voucher depending on the query options.
	 * @param voucherRequest the base query for the voucher
	 * @param product the voucher
	 * @param acceptedStatus the accepted status of a voucher
	 * @return the list of vouchers
	 */
	public List<Voucher> processProductVouchers(BaseVoucherRequest voucherRequest, Product product, String acceptedStatus);

}
