package com.isifid.stats.service.exception;

public class PartnerException extends Exception {
	public PartnerException(String message) {
		super(message);
	}
}
