package com.isifid.stats.service;

import com.isifid.stats.domain.response.AllVouchersResponse;
import com.isifid.stats.domain.request.AllVoucherRequest;
import com.isifid.stats.domain.response.PartnerVoucherResponse;
import com.isifid.stats.domain.request.VoucherRequest;
import com.isifid.stats.service.exception.PartnerException;

public interface PartnerVoucherService {

	/**
	 * Process all the vouchers of a partner.
	 * @param voucherRequest the request
	 * @return the list of vouchers of grouped by date of a partner
	 * @throws PartnerException if the partner id is not found in the database
	 */
	public PartnerVoucherResponse processVoucher(VoucherRequest voucherRequest) throws PartnerException;

	/**
	 * Process all the vouchers of all the partners.
	 * @param allVoucherRequest  the all voucher request
	 * @returna list of all the partners with all the vouchers
	 */
	public AllVouchersResponse processVoucher(AllVoucherRequest allVoucherRequest);
}
