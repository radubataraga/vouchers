package com.isifid.stats.utils;

import com.isifid.stats.domain.GroupBy;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {

	public static Integer getVoucherType(Integer voucherTypeId, Boolean isMaxiBon) {
		// Default 2 for voucher-type not maxi bon and voucher type 0
		Integer databaseVoucherType = 2;
		if (isMaxiBon != null) {
			if (voucherTypeId == 1 && !isMaxiBon) {
				databaseVoucherType = 0;
			} else if (voucherTypeId == 1 && isMaxiBon) {
				databaseVoucherType = 1;
			} else if (voucherTypeId == 2 && !isMaxiBon) {
				databaseVoucherType = 2;
			}
		} else {
			if (voucherTypeId == 1) {
				databaseVoucherType = 0;
			} else if (voucherTypeId == 2) {
				databaseVoucherType = 2;
			}
		}
		return databaseVoucherType;
	}

	public static boolean isFilteredByVoucherType(Integer requestVoucherType, Integer calculatedVoucherType) {
		if (requestVoucherType == null) {
			return true;
		}
		return requestVoucherType.equals(calculatedVoucherType);
	}

	public static Integer extractDateByGroup(Date usedDate, GroupBy requestGroupBy) {
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(usedDate);
		Integer category;
		switch (requestGroupBy) {
		case YEAR:
			category = calendar.get(Calendar.YEAR);
			break;
		case MONTH:
			category = calendar.get(Calendar.MONTH) + 1;
			break;
		case DAY:
			category = calendar.get(Calendar.DAY_OF_MONTH);
			break;
		default:
			category = calendar.get(Calendar.YEAR);
			break;
		}
		return category;
	}
}
