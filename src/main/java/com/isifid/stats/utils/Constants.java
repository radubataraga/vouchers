package com.isifid.stats.utils;

public class Constants {

	//Endpoints
	public static final String USE_ONE_ENDPOINT = "/use/one";
	public static final String USE_ALL_ENDPOINT = "/use/all";
	public static final String CACHE_USE_ONE_ENDPOINT = "/cache/use/one";
	public static final String CACHE_USE_ALL_ENDPOINT = "/cache/use/all";

	//Cache names
	public static final String ONE_VOUCHER_CACHE_NAME = "vouchers";
	public static final String ALL_VOUCHERS_CACHE_NAME = "all-vouchers";

	public static final String ACTIVE_PRODUCTS = "ACTIVE";
	public static final String PARTNER_ID_ERROR_MESSAGE = "Partner id doesn't exist";
	public static final String DATE_FORMAT = "yyyy-MM-dd";

}
