package com.isifid.stats.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "PRODUCTS")
public class Product {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "STATUS")
	private String status;

	@Column(name = "PRICE", nullable = false)
	private Double amount;

	@Column(name = "voucher_type_id")
	private Integer voucherTypeId;

	@Column(name = "IS_MAXI_BON", nullable = true)
	private Boolean isMaxiBon;

	@Column(name ="PARTNER_ID")
	@NotNull
	private Integer partnerId;

	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "PRODUCT_ID", updatable = false, insertable = false)
	private Set<Item> items = new HashSet<>();

	public Product() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Set<Item> getItems() {
		return items;
	}

	public void setItems(Set<Item> items) {
		this.items = items;
	}

	public Double getAmount() {
		return amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Integer getVoucherTypeId() {
		return voucherTypeId;
	}

	public void setVoucherTypeId(Integer voucherTypeId) {
		this.voucherTypeId = voucherTypeId;
	}

	public Integer getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public Boolean getMaxiBon() {
		return isMaxiBon;
	}

	public void setMaxiBon(Boolean maxiBon) {
		isMaxiBon = maxiBon;
	}

	public void setPartnerId(Integer partnerId) {
		this.partnerId = partnerId;
	}

	@Override
	public String toString() {
		return "Product{" + "id=" + id + ", status='" + status + '\'' + '}';
	}
}
