package com.isifid.stats.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "voucher_type")
public class VoucherType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	public VoucherType() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "VoucherType{" + "id=" + id + '}';
	}
}
