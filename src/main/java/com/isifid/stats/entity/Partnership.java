package com.isifid.stats.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "PARTNERSHIPS")
public class Partnership {

//	@Id
//	@GeneratedValue(strategy = GenerationType.IDENTITY)
//	private int id;

	//	@OneToOne(optional = false)
	//	@JoinColumn(name = "PARTNER_ID", nullable = false)
	//	private Partner partner;

	@Id
	@Column(name = "PARTNER_ID")
	private int partnerId;

	@Column(name = "FUNDING_RATE")
	private Integer fundingRate;

	@Column(name = "FUNDING_RATE_PREMIUM")
	private Integer fundingRatePremium;

	public Partnership() {
	}

	public int getPartnerId() {
		return partnerId;
	}

	public void setPartnerId(int partnerId) {
		this.partnerId = partnerId;
	}

	public Integer getFundingRate() {
		return fundingRate;
	}

	public void setFundingRate(Integer fundingRate) {
		this.fundingRate = fundingRate;
	}

	public Integer getFundingRatePremium() {
		return fundingRatePremium;
	}

	public void setFundingRatePremium(Integer fundingRatePremium) {
		this.fundingRatePremium = fundingRatePremium;
	}

	@Override
	public String toString() {
		return "Partnership{" + "partnerId=" + partnerId + ", fundingRate=" + fundingRate + ", fundingRatePremium=" + fundingRatePremium + '}';
	}
}
