package com.isifid.stats.controller;

import com.isifid.stats.service.exception.PartnerException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.isifid.stats.utils.Constants.PARTNER_ID_ERROR_MESSAGE;

@ControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler(value = PartnerException.class)
	public ResponseEntity<String> inputValidationError() {
		return new ResponseEntity<>(PARTNER_ID_ERROR_MESSAGE, HttpStatus.NOT_FOUND);
	}

	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Map<String, String>> handleValidationExceptions(MethodArgumentNotValidException ex) {

		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			if (error instanceof FieldError) {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errors.put(fieldName, errorMessage);
			} else {
				errors.put(error.getObjectName(), error.getDefaultMessage());
			}
		});
		return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
	}
}
