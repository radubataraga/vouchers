package com.isifid.stats.controller;

import com.isifid.stats.domain.request.AllVoucherRequest;
import com.isifid.stats.domain.request.VoucherRequest;
import com.isifid.stats.domain.response.AllVouchersResponse;
import com.isifid.stats.domain.response.PartnerVoucherResponse;
import com.isifid.stats.service.PartnerVoucherService;
import com.isifid.stats.service.exception.PartnerException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static com.isifid.stats.utils.Constants.CACHE_USE_ALL_ENDPOINT;
import static com.isifid.stats.utils.Constants.CACHE_USE_ONE_ENDPOINT;

@RestController
public class CacheVoucherController {

	private Logger logger = LoggerFactory.getLogger(VoucherController.class);

	@Autowired
	@Qualifier("cacheablePartnerVoucherService")
	private PartnerVoucherService partnerVoucherService;

	@CrossOrigin(origins = "*")
	@ApiOperation(value = "One voucher")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The vouchers of a partner grouped by date"),
			@ApiResponse(code = 400, message = " Error if missing a mandatory parameter or wrong dates"),
			@ApiResponse(code = 404, message = "Partner id not found") })
	@RequestMapping(value = CACHE_USE_ONE_ENDPOINT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<PartnerVoucherResponse> useVoucher(@Valid @RequestBody VoucherRequest request) throws PartnerException {
		logger.info("POST Voucher request received {}", request);

		PartnerVoucherResponse partnerVoucherResponse = partnerVoucherService.processVoucher(request);

		logger.info("POST Voucher response received {}", partnerVoucherResponse);
		return ResponseEntity.ok().body(partnerVoucherResponse);
	}

	@CrossOrigin(origins = "*")
	@ApiOperation(value = "All vouchers")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "The vouchers of all partner grouped by date"),
			@ApiResponse(code = 400, message = " Error if missing a mandatory parameter or wrong dates") })
	@RequestMapping(value = CACHE_USE_ALL_ENDPOINT, method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<AllVouchersResponse> useAllVoucher(@Valid @RequestBody AllVoucherRequest request) {
		logger.info("POST Voucher request received {}", request);

		AllVouchersResponse partnerVoucher = partnerVoucherService.processVoucher(request);

		logger.info("POST Voucher response received {}", partnerVoucher);
		return ResponseEntity.ok().body(partnerVoucher);
	}
}
