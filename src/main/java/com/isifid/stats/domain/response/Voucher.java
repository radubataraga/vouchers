package com.isifid.stats.domain.response;


public class Voucher {

	private Integer voucher_type;
	private Integer voucher_use;
	private Double voucher_amount;
	private String date;
	private Integer cofinancing_rate;

	public Integer getVoucher_type() {
		return voucher_type;
	}

	public void setVoucher_type(Integer voucher_type) {
		this.voucher_type = voucher_type;
	}

	public Integer getVoucher_use() {
		return voucher_use;
	}

	public void setVoucher_use(Integer voucher_use) {
		this.voucher_use = voucher_use;
	}

	public Double getVoucher_amount() {
		return voucher_amount;
	}

	public void setVoucher_amount(Double voucher_amount) {
		this.voucher_amount = voucher_amount;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public Integer getCofinancing_rate() {
		return cofinancing_rate;
	}

	public void setCofinancing_rate(Integer cofinancing_rate) {
		this.cofinancing_rate = cofinancing_rate;
	}
}
