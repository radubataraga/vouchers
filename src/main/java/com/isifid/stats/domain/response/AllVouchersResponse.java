package com.isifid.stats.domain.response;

import java.util.List;

public class AllVouchersResponse {

	List<PartnerVoucherResponse> partners;

	public AllVouchersResponse() {
	}

	public List<PartnerVoucherResponse> getPartners() {
		return partners;
	}

	public void setPartners(List<PartnerVoucherResponse> partners) {
		this.partners = partners;
	}
}
