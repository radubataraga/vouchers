package com.isifid.stats.domain.response;

import com.isifid.stats.domain.GroupBy;

import java.util.ArrayList;
import java.util.List;

public class PartnerVoucherResponse {

	private int partner_id;
	private String partner_name;
	private List<Voucher> vouchers;
	private GroupBy group_by;

	public PartnerVoucherResponse() {
		vouchers = new ArrayList<>();
	}

	public int getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(int partner_id) {
		this.partner_id = partner_id;
	}

	public String getPartner_name() {
		return partner_name;
	}

	public void setPartner_name(String partner_name) {
		this.partner_name = partner_name;
	}

	public List<Voucher> getVouchers() {
		return vouchers;
	}

	public void setVouchers(List<Voucher> vouchers) {
		this.vouchers = vouchers;
	}

	public GroupBy getGroup_by() {
		return group_by;
	}

	public void setGroup_by(GroupBy group_by) {
		this.group_by = group_by;
	}
}
