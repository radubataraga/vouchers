package com.isifid.stats.domain.request;

import com.isifid.stats.domain.GroupBy;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.Objects;

@ConsistentDateParameters
public class BaseVoucherRequest {

//	@Size(min = 0, max = 2)
	private Integer voucher_type;
	@NotNull
	private Date date_min;
	@NotNull
	private Date date_max;
	@NotNull
	private GroupBy group_by;

	public Integer getVoucher_type() {
		return voucher_type;
	}

	public void setVoucher_type(Integer voucher_type) {
		this.voucher_type = voucher_type;
	}

	public Date getDate_min() {
		return date_min;
	}

	public void setDate_min(Date date_min) {
		this.date_min = date_min;
	}

	public Date getDate_max() {
		return date_max;
	}

	public void setDate_max(Date date_max) {
		this.date_max = date_max;
	}

	public GroupBy getGroup_by() {
		return group_by;
	}

	public void setGroup_by(GroupBy group_by) {
		this.group_by = group_by;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof BaseVoucherRequest))
			return false;
		BaseVoucherRequest that = (BaseVoucherRequest) o;
		return Objects.equals(getVoucher_type(), that.getVoucher_type()) && getDate_min().equals(that.getDate_min()) && getDate_max()
				.equals(that.getDate_max()) && getGroup_by() == that.getGroup_by();
	}

	@Override
	public int hashCode() {
		return Objects.hash(getVoucher_type(), getDate_min(), getDate_max(), getGroup_by());
	}
}
