package com.isifid.stats.domain.request;


import javax.validation.constraints.NotNull;
import java.util.Objects;

public class VoucherRequest extends BaseVoucherRequest {

	@NotNull
	private int partner_id;
//	private VoucherType voucher_type;

	public Integer getPartner_id() {
		return partner_id;
	}

	public void setPartner_id(Integer partner_id) {
		this.partner_id = partner_id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof VoucherRequest))
			return false;
		if (!super.equals(o))
			return false;
		VoucherRequest that = (VoucherRequest) o;
		return getPartner_id().equals(that.getPartner_id());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getPartner_id());
	}
}
