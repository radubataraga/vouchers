package com.isifid.stats.domain.request;

import java.util.Objects;

public class AllVoucherRequest extends BaseVoucherRequest {

	private String status_id;

	public AllVoucherRequest() {
	}

	public String getStatus_id() {
		return status_id;
	}

	public void setStatus_id(String status_id) {
		this.status_id = status_id;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (!(o instanceof AllVoucherRequest))
			return false;
		if (!super.equals(o))
			return false;
		AllVoucherRequest that = (AllVoucherRequest) o;
		return Objects.equals(getStatus_id(), that.getStatus_id());
	}

	@Override
	public int hashCode() {
		return Objects.hash(super.hashCode(), getStatus_id());
	}
}
