package com.isifid.stats.repository;

import com.isifid.stats.entity.Partnership;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PartnershipRepository extends JpaRepository<Partnership, Integer> {

	public Optional<Partnership> findByPartnerId(Integer partnerId);
}
