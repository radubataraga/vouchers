package com.isifid.stats.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class SwaggerConfig extends WebMvcConfigurationSupport {

    @Value("${my.swaggerTitle}")
    private String swaggerTitle;
    @Value("${my.swaggerResume}")
    private String swaggerResume;
    @Value("${my.swaggerVersion}")
    private String swaggerVersion;
    @Value("${my.swaggerTOS}")
    private String swaggerTOS;
    @Value("${my.swaggerContactName}")
    private String swaggerContactName;
    @Value("${my.swaggerContacUrl}")
    private String swaggerContacUrl;
    @Value("${my.swaggerContacEmail}")
    private String swaggerContacEmail;
    @Value("${my.swaggerLicence}")
    private String swaggerLicence;
    @Value("${my.swaggerLicenceUrl}")
    private String swaggerLicenceUrl;

    @Bean
    public Docket taskApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName(swaggerTitle)
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo())
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.regex("(?!/error).+"))
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title(swaggerTitle)
                .description(swaggerResume)
                .version(swaggerVersion)
                .license(swaggerLicence)
                .licenseUrl(swaggerLicenceUrl)
                .build();
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");

        super.addResourceHandlers(registry);
    }
}